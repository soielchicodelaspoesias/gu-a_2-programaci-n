#!/usr/bin/env python
# -*- coding: utf-8 -*-

#se crea una matriz de tamaño n x (2n) y se rellena con puros asteriscos
def crear_matriz(n, matriz):
    for i in range(n):
        matriz.append([0] * (n * 2))
    for i in range(n):
        for j in range(n * 2):
            matriz[i][j] = '*'


# funcion que imprime la matriz
def imprimir_matriz(n, matriz, texto):
    for i in range(n):
        for j in range(n * 2):
            texto += matriz[i][j] + ' '
        texto += ' \n'
    print(texto)

""""" se crea un triangulo dentro de la matriz, primero se divide en 2 y se trabaja la primera mitad de 0 hasta n
despues se trabaja con la otra parte de n hasta 2n"""
def crear_triangulo(n, matriz):
    for i in range(0, n):
        #las posiciones debajo de la matriz se combierten en espacios
        for j in range(0, i + 1, 1):
            matriz[i][j] = ' '
        """"" combiente la segunda parte de la matriz en puros espacios y despues las posiciones que estan debajo de la 
        diagonal insersa a la anterior se transforman en '*' """""
        for j in range(n, n * 2):
            matriz[i][j] = ' '
        for j in range(n, (n * 2) - i - 1):
            matriz[i][j] = '*'

# main
n = int(input("Ingrese la altura del triangulo: "))
matriz = []
texto = ""
crear_matriz(n, matriz)
crear_triangulo(n, matriz)
imprimir_matriz(n, matriz, texto)
"""""
es programa no es como el ejemplo del ejercicio por que encontre que esteticamente se ve mejor asi
"""
