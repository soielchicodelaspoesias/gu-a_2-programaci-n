import random

# el usuario ingresa el tamaño de la lista
n = int(input("Ingrese el tamaño de la lista: "))
lista = []
temporal = 0
# se crea la lista de tamaño n con numeros aleatorios del 0 al 39
for i in range(n):
    lista.append(random.randrange(40))

print("La lista es la siguiente: ", lista)
# ordenamiento burbuja
for i in range(n):
    for j in range(n - 1):
        if lista[i] < lista[j]:
            temporal = lista[i]
            lista[i] = lista[j]
            lista[j] = temporal

print("La lista ordenada de forma ascendiente: ", lista)
