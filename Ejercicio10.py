
#mensaje a decifrar
string = "XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
print("El mensaje a desifrar es el siguiente: ", string)
# del mensaje encriptado se eliminan las "X" y se guarda en la variable mensaje
mensaje = string.replace("X", "")
print("........")
# se imprime el string mensaje pero al reves
print("Por fin he desifrado el mensaje oculto y dice lo siguiente: ", mensaje[::-1])

