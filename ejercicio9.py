#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

""" Funcion que crea la matriz y la llena de ceros y la diagonal de unos, los
ceros y los uno son caracteres para colocarlos dentro de un str para que
se vea de mejor manera al momento de imprimir la matriz """

def crear_matriz(n, matriz):
    for i in range(n):
        matriz.append([0] * n)
    for i in range(n):
        for j in range(n):
            matriz[i][j] = '0'
            if i == j:
                matriz[i][j] = '1'

# funcion que imprime la matriz
def imprimir_matriz(n, matriz, texto):
    for i in range(n):
        for j in range(n):
            texto += matriz[i][j] + ' '
        texto += ' \n'
    print(texto)


# main
n = int(input("Ingrese las dimensiones: "))
matriz = []
texto = ""
crear_matriz(n, matriz)
imprimir_matriz(n, matriz, texto)




