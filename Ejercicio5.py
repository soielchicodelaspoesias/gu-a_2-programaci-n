import random

t = int(input("Ingrese el tamaño de las listas: "))
a = []
b = []
c = []
n = []
# se generan las listas de tamaño n con numeros aleatorios de 0 al 39
for i in range(t):
    a.append(random.randrange(40))
    b.append(random.randrange(40))
    c.append(random.randrange(40))
print('Las listas son las siguientes: ')
print(a)
print(b)
print(c)
# se suman las listas y con el comando set se eliminan los repetidos
n = (a + b + c)
print("unica lista la cual es la suma de las 3 anteriones y se elminaron los elemntos repetidos: ")
print(set(n))
# si el numero es par distinto de 0 y si termina en 0 se cambia por un numero aleatorio
for i in range(t):
    if n[i] % 2 == 0 and n[i] % 10 != 0 and n[i] != 0:
        n[i] = random.randrange(40)
print(set(n))
