""" encuentra los numeros menores, iguales y mayores que k dentro de la lista y se guardan los valores dentro de otra
lista, si al final del ciclo esta vacia significa que no hay numeros mayores o menores dependiendo del caso sino se
imprime la lista"""
def mayores_menores_iguales(a, k, iguales_k, menores_que_k, mayores_que_k):
    for i in range(len(a)):
        if a[i] == k:
            iguales_k.append(a[i])
        if a[i] < k:
            menores_que_k.append(a[i])
        if a[i] > k:
            mayores_que_k.append(a[i])
    if len( menores_que_k) == 0:
        print("No hay numeros menores que k")
    else:
        print("Los numeros menores a k son: ", menores_que_k)
    if len(mayores_que_k) == 0:
        print("No hay numeros mayores que k")
    else:
        print("Los numeros mayores a k son: ", mayores_que_k)
    print("Los numeros iguales a k son: ", iguales_k)


# se encuentran los numeros multiplos a k y se guardan detro de una lista y depues se imprime esa lista
def multiplos(a, k, multiplos_de_k):
    for i in range(len(a)):
        if a[i] % k == 0:
            multiplos_de_k.append(a[i])
    print("Los numeros multiplos de k son: ", multiplos_de_k)


# se crea una lista con numeros del 1 al 20 y seimprime
a = [x for x in range(1, 21)]
print("La lista de numeros enteros es la siguiente: ", a)

# se ingresa el valor de k, el numero que se comparara
k = int(input("Ingrese el valor de k: "))

# se definen las listas y se llaman las funciones
mayores_que_k = []
menores_que_k = []
iguales_k = []
multiplos_de_k = []
mayores_menores_iguales(a, k, iguales_k, menores_que_k, mayores_que_k)
multiplos(a, k, multiplos_de_k)


